//
//  FluxDomain.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 9/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

struct FluxDomain {
    
    /// The Flux hostname
    let hostname: String = fluxHostname
    
    /// URL for the phone commit POST request
    var phoneCommit: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v1/app_rego/phone_commit")
    }
    
    /// URL to verify the phone using an SMS code and nonce
    var phoneVerify: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v1/app_rego/phone_verify")
    }
    
    ///
    var registerInitialEmail: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v0/register/initial_email")
    }
    
    ///
    var registerAllAtOnce: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v0/register/all_at_once")
    }
    
    ///
    var updateDetails: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v0/user_details")
    }
    
    /// Request magic link
    var magicLink: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v1/app/new_magic_link")
    }
    
    var feedback: URL? {
        return URL(string: hostname)?.appendingPathComponent("/api/v1/app/feedback")
    }
    
    /// Suburbs URL
    func getSuburbsURL(country: String, postcode: String) -> URL? {
        let path = String(format: "/api/v0/get_suburbs/%@/%@", arguments: [country, postcode])
        return URL(string: hostname)?.appendingPathComponent(path)
    }
    
    /// Get streets in postcode
    func getStreetsURL(country: String, postcode: String, suburb: String) -> URL? {
        let path = String(format: "/api/v0/get_streets/%@/%@/%@", arguments: [country, postcode, suburb])
        return URL(string: hostname)?.appendingPathComponent(path)
    }
    
    /// URL to sign in via magic link
    func getSignInMagicLink(withToken token: String) -> URL? {
        let path = String(format: "/api/v1/app/login_magic_link/%@", arguments: [token])
        return URL(string: hostname)?.appendingPathComponent(path)
    }
    
    ///
    func login(withMagicToken token: String) -> URL? {
        let path = String(format: "/api/v1/app/login_magic_link/%@", arguments: [token])
        return URL(string: hostname)?.appendingPathComponent(path)
    }
    
}

let fluxDomain = FluxDomain()
