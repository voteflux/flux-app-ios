//
//  CustomiseMembershipViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 2/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class CustomiseMembershipViewController: UIViewController {

    @IBOutlet weak var volunteerSwitch: UISwitch!
    @IBOutlet weak var candidateSwitch: UISwitch!
    @IBOutlet weak var organiseSwitch: UISwitch!
    @IBOutlet weak var memberSwitch: UISwitch!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleDecorationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var decorationTopPadding: NSLayoutConstraint!
    @IBOutlet weak var descriptionTopPadding: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor
        
        
        // Do any additional setup after loading the view.
        if isNewMember {
            titleLabel.text = "You're now a member!"
            descriptionLabel.text = "Welcome to Flux!\nCustomise your membership"
            descriptionTopPadding.constant = 192
            titleDecorationLabel.isHidden = false
            view.layoutIfNeeded()
        } else {
            titleLabel.text = "Welcome back!"
            descriptionLabel.text = "Customise your membership"
            descriptionTopPadding.constant = 122
            titleDecorationLabel.isHidden = true
            view.layoutIfNeeded()
        }
    }
    
    /// Populates fields with stored values
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        volunteerSwitch.isOn = UserPreferences.getPreference(forOption: .volunteer)
        candidateSwitch.isOn = UserPreferences.getPreference(forOption: .candidate)
        organiseSwitch.isOn = UserPreferences.getPreference(forOption: .organiser)
        memberSwitch.isOn = UserPreferences.getPreference(forOption: .member)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isNewMember = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateDetails() {
        let volunteer: Bool = volunteerSwitch.isOn
        let candidate: Bool = organiseSwitch.isOn
        let organise: Bool = organiseSwitch.isOn
        let member: Bool = memberSwitch.isOn
        
        LoadingIndicator.standard.show(onView: view)
        FluxAPI.updateMembershipDetails(volunteer: volunteer, candidate: candidate, organise: organise, member: member, onSuccess: onSuccess, onFail: onFail)
    }

    func onSuccess(preferences: NSDictionary) {
        setCustomisedBefore(true)
        UserPreferences.storeUserPreferences(preferences: preferences)
        LoadingIndicator.standard.hide()
        performSegue(withIdentifier: "updated", sender: nil)
    }
    
    func onFail(message: String?) {
        LoadingIndicator.standard.hide()
        presentError(title: errorTitle, message: message, close: errorClose)
    }

}
