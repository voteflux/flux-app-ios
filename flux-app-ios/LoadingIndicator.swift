//
//  LoadingIndicator.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 7/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

class LoadingIndicator {
    
    static let standard = LoadingIndicator()
    
    let container: UIView
    let loadingView: UIView
    let indicator: UIActivityIndicatorView
    
    init() {
        self.container = UIView()
        self.loadingView = UIView()
        self.indicator = UIActivityIndicatorView()
    
    }
    
    func show(onView view: UIView) {
        container.frame = view.frame
        container.center = view.center
        container.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor(red: 0.26, green: 0.26, blue: 0.26, alpha: 0.6)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(indicator)
        container.addSubview(loadingView)
        view.addSubview(container)
        indicator.startAnimating()
    }
    
    func hide() {
        indicator.stopAnimating()
        container.removeFromSuperview()
    }
    
}
