//
//  FluxAPI.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 2/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation
import Alamofire
import Security

// Makes FluxAPI calls. Usually take required parameters and success and fail callbacks.
// 
// Tried not to put any logic in here.

/// A collection of Flux API calls.
class FluxAPI {
    
    /// Requests SMS code to be sent to phone for verification. On success passes phone, on fail pass error message.
    static func requestCommitPhone(phone: TelephoneNumber, onSuccess: @escaping (TelephoneNumber)->Void, onFail: @escaping (String?)->Void) {
        let parameters: [String: AnyObject] = [
            PhoneVerificationKeys.PhoneNumber.rawValue: phone.internationalNumber as AnyObject
        ]
        if let requestURL = fluxDomain.phoneCommit {
            Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                if let status = JSON?["status"] as? String, status == "okay" {
                    onSuccess(phone)
                } else {
                    onFail(nil)
                }
            }
        } else {
            onFail(nil)
        }
    }
    
    /// Confirms verfication of phone
    static func verifyPhone(phone: TelephoneNumber, code: String, nonce: String, onSuccess: @escaping (TelephoneNumber, String)->Void, onFail: @escaping (String?)->Void) {
        if let requestURL = fluxDomain.phoneVerify {
            let parameters: [String: String] = [
                PhoneVerificationKeys.PhoneNumber.rawValue: phone.internationalNumber,
                PhoneVerificationKeys.SMSCode.rawValue: code,
                PhoneVerificationKeys.Nonce.rawValue: nonce
            ]
//            let data = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
//            let json = String(data: data, encoding: String.Encoding.utf8)
//            print(json ?? "")
            Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                if JSON?["error"] == nil {
                    onSuccess(phone, nonce)
                } else {
                    onFail(JSON?["error"] as? String)
                }
            }
        } else {
            onFail(nil)
        }
    }

    /// Registers user.
    static func registerAllAtOnce(form: [String: AnyObject], onSuccess: @escaping (NSDictionary)->Void, onFail: @escaping (String?)->Void) {
        if let requestURL = fluxDomain.registerAllAtOnce {
//            let data = try! JSONSerialization.data(withJSONObject: form, options: .prettyPrinted)
//            let json = String(data: data, encoding: String.Encoding.utf8)
//            print(json ?? "")
            Alamofire.request(requestURL, method: .post, parameters: form, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                if let JSON = JSON, let s = JSON["s"] as? String, updateToken(s) {
                    onSuccess((JSON["user"] as? NSDictionary) ?? NSDictionary())
                } else {
                    onFail(JSON?["error"] as? String)
                }
            }
        } else {
            onFail(nil)
        }
    }
   
    static func querySuburbs(country: String, postcode: String, onSuccess: @escaping ([String])->Void, onFail: ((String?)->Void)?) {
        if let requestURL = fluxDomain.getSuburbsURL(country: country, postcode: postcode) {
            Alamofire.request(requestURL, method: .get).responseJSON { response in
                let JSON = response.result.value as? NSDictionary
                if let status = JSON?["status"] as? String, status == "okay", let suburbs = JSON?["suburbs"] as? [String] {
                    onSuccess(suburbs)
                } else {
                    onFail?(nil)
                }
            }
        } else {
            onFail?(nil)
        }
    }
    
    static func queryStreets(country: String, postcode: String, suburb: String, onSuccess: @escaping ([String])->Void, onFail: ((String?)->Void)?) {
        if let requestURL = fluxDomain.getStreetsURL(country: country, postcode: postcode, suburb: suburb) {
            Alamofire.request(requestURL, method: .get).responseJSON { response in
                if let JSON = response.result.value as? NSDictionary,
                    let status = JSON["status"] as? String, status == "okay" {
                    if let streets = JSON["streets"] as? [String] {
                        onSuccess(streets)
                    } else {
                        onFail?(nil)
                    }
                } else {
                    onFail?(nil)
                }
            }
        } else {
            onFail?(nil)
        }
    }
    
    /// Generates a cryptographically secure random number, returned as hex string
    static func generateNonce() -> String {
        let bytesCount = 32
        var randomNumber = ""
        var randomBytes = [UInt8](repeating: 0, count: bytesCount)
        
        _ = SecRandomCopyBytes(kSecRandomDefault, bytesCount, &randomBytes)
        
        randomNumber = randomBytes.map({String(format: "%02hhx", $0)}).joined(separator: "")
        
        return randomNumber
    }
    
    static func updateMembershipDetails(volunteer: Bool?, candidate: Bool?, organise: Bool?, member: Bool?, onSuccess: ((NSDictionary)->Void)?, onFail: ((String?)->Void)?) {
        
        if let requestURL = fluxDomain.updateDetails {
            // Check
            if volunteer == nil || candidate == nil || organise == nil || member == nil {
                return
            }
            var parameters = [String: AnyObject]()
            if let volunteer = volunteer {
                parameters[CustomiseMembershipKeys.Volunteer.rawValue] = String(volunteer) as AnyObject
            }
            if let candidate = candidate {
                parameters[CustomiseMembershipKeys.Candidate.Federal.rawValue] = String(candidate) as AnyObject
                parameters[CustomiseMembershipKeys.Candidate.State.rawValue] = String(candidate) as AnyObject
                parameters[CustomiseMembershipKeys.Candidate.Local.rawValue] = String(candidate) as AnyObject
            }
            if let organise = organise {
                parameters[CustomiseMembershipKeys.Organise.rawValue] = String(organise) as AnyObject
            }
            if let member = member {
                parameters[CustomiseMembershipKeys.Member.rawValue] = String(member) as AnyObject
            }
            
            if let s = retrieveToken() as AnyObject? {
                parameters["s"] = s
            } else {
                //no stored credentials...
                debugPrint("API call failed. No stored credentials.")
                return
            }
            let data = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            let json = String(data: data, encoding: String.Encoding.utf8)
            var request = Alamofire.URLRequest(url: requestURL)
            
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = data
            
            print(json ?? "")
            Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                if let message = JSON?["error"] as? String {
                    onFail?(message)
                } else {
                    onSuccess?( (JSON != nil) ? JSON! : NSDictionary() )
                }
            }
        } else {
            onFail?(nil)
        }
    }
    
    static func requestMagicLink(toEmail email: String, onSuccess: @escaping (String)->Void, onFail: @escaping (String?)->Void) {
        
        if let requestURL = fluxDomain.magicLink {
            let parameters: [String: AnyObject] = [
                MagicLinkKeys.Email.rawValue: email as AnyObject
            ]
            
            Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                print(JSON?["error"] as? String ?? "")
                
                if let statusCode = response.response?.statusCode, statusCode == 200 && JSON?["error"] == nil {
                    onSuccess(email)
                } else {
                    onFail(JSON?["error"] as? String ?? "")
                }
            }
        } else {
            onFail(nil)
        }
    }
    
    static func login(withMagicToken token: String, onFail: ((String?)->Void)?, dispatchGroup group: DispatchGroup?) {
        
        if let requestURL = fluxDomain.login(withMagicToken: token) {
            Alamofire.request(requestURL).responseJSON { response in
                
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                print(JSON?["error"] as? String ?? "")
                
                if let s = JSON?["s"] as? String, let status = JSON?["status"] as? String, status == "okay" {
                    let preferences: NSDictionary = (JSON?["user"] as? NSDictionary) ?? NSDictionary()
                    Session.login(s: s, preferences: preferences, dispatchedInGroup: group, animated: true)
                    isNewMember = false
                } else {
                    onFail?(JSON?["error"] as? String)
                    group?.leave()
                }
            }
        } else {
            onFail?(nil)
        }
    }
    
    static func send(feedback: String, onSuccess: @escaping ()->Void, onFail: @escaping (String?)->Void) {
        if let requestURL = fluxDomain.feedback {
            let parameters: [String: AnyObject]
            if let token = retrieveToken() {
                parameters = [
                    FeedbackKeys.feedback.rawValue: feedback as AnyObject,
                    FeedbackKeys.token.rawValue: token as AnyObject
                ]
            } else {
                debugPrint("Send Feedback failed. No stored credentials")
                onFail("")
                return
            }
            
            Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                let JSON: NSDictionary? = response.result.value as? NSDictionary
                
                if let status = JSON?["status"] as? String, status == "okay" {
                    onSuccess()
                } else {
                    onFail(JSON?["error"] as? String ?? "")
                }
            }
        } else {
            onFail(nil)
        }
        
    }
    
}
