//
//  Authentication.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 3/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

func updateToken(_ token: String) -> Bool {
    return KeychainWrapper.standard.set(token, forKey: KeychainKeys.s)
}

func retrieveToken() -> String? {
    return KeychainWrapper.standard.string(forKey: KeychainKeys.s)
}

func removeToken() -> Bool {
    return KeychainWrapper.standard.removeObject(forKey: KeychainKeys.s)
}
