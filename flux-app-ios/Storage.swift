//
//  Storage.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 8/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

var isNewMember: Bool {
    get {
        return UserDefaults.standard.bool(forKey: "isNewMember")
    }
    set {
        UserDefaults.standard.set(newValue, forKey: "isNewMember")
        _ = UserDefaults.standard.synchronize()
    }
}

func hasCustomisedBefore() -> Bool {
    return UserDefaults.standard.bool(forKey: "hasCustomisedBefore")
}

func setCustomisedBefore(_ value: Bool) {
    UserDefaults.standard.set(value, forKey: "hasCustomisedBefore")
    _ = UserDefaults.standard.synchronize()
}

func hasAppAlreadyLaunchedOnce() -> Bool {
    return UserDefaults.standard.bool(forKey: "hasAppAlreadyLaunchedOnce")
}

func setHasAppAlreadyLaunchedOnce(_ value: Bool) {
    UserDefaults.standard.set(value, forKey: "hasAppAlreadyLaunchedOnce")
}

protocol UserPreferenceType {}

struct UserPreferences {
    
    /// Options that can be handled
    enum Options: String {
        case volunteer = "volunteer"
        case candidate = "candidature_federal"
        case organiser = "vol_organise"
        case member = "state_consent"
    }
    
    static func getPreference(forOption option: Options) -> Bool {
        switch option {
        case .volunteer,
             .candidate,
             .organiser,
             .member:
            return UserDefaults.standard.bool(forKey: option.rawValue)
        }
    }
    
    /// Set single user preference
    static func setPreference(forOption option: Options, value: Bool) {
        UserDefaults.standard.set(value, forKey: option.rawValue)
        _ = UserDefaults.standard.synchronize()
    }
    
    /// Stores user details
    static func storeUserPreferences(preferences: NSDictionary) {
        // Options that can be handled
        let keys = Set<Options>(preferences.allKeys.map({ $0 as? String }).filter({ $0 != nil }).map({ Options.init(rawValue: $0!) }).filter({ $0 != nil }).map({ $0! }))
        // Set value for handled options
        for key in keys {
            if let value = preferences[key.rawValue] as? Bool {
                setPreference(forOption: key, value: value)
            } else if let v = preferences[key.rawValue] as? String, let value = Bool(v) {
                setPreference(forOption: key, value: value)
            }
        }
    }
    
    /// Removes every object in UserDefaults.standard for every key registered by app.
    static func clearPreferences() {
        if let name = Bundle.main.bundleIdentifier {
            UserDefaults.standard.setPersistentDomain([String: Any](), forName: name)
        } else {
            debugPrint("Unable to remove preferences. ")
        }
    }
    
}
