//
//  Styles.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 2/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation
import UIKit

struct Styles {
    
    var highlightColor = UIColor(red:0.89, green:0.35, blue:0.05, alpha:1.0)
    var backgroundColor = UIColor(red:0.13, green:0.13, blue:0.17, alpha:1.0)
    
    var borderRadius: CGFloat = 1
    
    
    // MARK: Your Phone
    
    ///
    var yourPhone_widthMultiplier: CGFloat = 1.3
    var yourPhone_heightMultiplier: CGFloat = 0.8
    
}

struct Colors {
    var highlightColor = UIColor(red:227/255, green:88/255, blue:13/255, alpha:1.0)
    var backgroundColor = UIColor(red:33/255, green:33/255, blue:43/255, alpha:1.0)
    var titleColor = UIColor(red: 108/255, green: 108/255, blue: 108/255, alpha: 1)
}
let colors = Colors()

var styles = Styles()


func style() {
    
//    UIButton.appearance().backgroundColor = colors.highlightColor
//    UIButton.appearance().layer.cornerRadius = 5
    
}
