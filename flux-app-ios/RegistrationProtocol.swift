//
//  RegistrationProtocol.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 2/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

protocol RegistrationProtocol {
    
    var form: RegistrationForm { get set }
    
    var addressVersion: String { get }
    
    /// Current phone number
    var phone: TelephoneNumber? { get set }
    
    /// Randomly generated token
    var nonce: String? { get set }
    
    // MARK: Personal details
    
    var firstName: String? { get set }
    var middleNames: String? { get set }
    var lastName: String? { get set }
    
    var email: String? { get set }
    
    // MARK: Address details
    
    var postcode: String? { get set }
    var suburb: String? { get set }
    var country: String? { get set }
    var streetName: String? { get set }
    var streetNumber: String? { get set }
    
    var DOBYear: String? { get set }
    var DOBMonth: String? { get set }
    var DOBDay: String? { get set }
    
    var isRegistered: String? { get set }
    
    // MARK: Methods
    
    /// Stored phone is the verified phone
    func phoneIsVerified() -> Bool
    
    /// Set verified phone. Phone and nonce will be either both be defined or both undefined.
    func setVerified(phone: TelephoneNumber?, nonce: String?)
    
    /// Client-side checks
    func isReadyToRegister() -> Bool
    
}
