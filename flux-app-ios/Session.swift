//
//  Session.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 9/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

struct Session {
    
    /// Actions performed after succesful login. Updates token, user preferences.
    static func loginSuccess(s: String, preferences: NSDictionary) {
        // store token
        _ = updateToken(s)
        
        // update preferences
        UserPreferences.storeUserPreferences(preferences: preferences)
    }
    
    /// Take user to home screen. If opening is present, let it take us there, otherwise replace rootVC.
    static func login(s: String, preferences: NSDictionary, dispatchedInGroup group: DispatchGroup?, animated: Bool = true) {
        DispatchQueue.main.async(execute: {
            // Store token
            _ = updateToken(s)
            // Update preferences
            UserPreferences.storeUserPreferences(preferences: preferences)
            // Present resulting VC
            if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
                if window.rootViewController?.presentedViewController as? OpeningViewController == nil {
                    if hasCustomisedBefore() {
                        let home = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as? HomeTabBarViewController
                        window.rootViewController = home // ?.present(home, animated: animated, completion: nil)
                    } else {
                        let customise = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CustomiseMembership")
                        window.rootViewController = customise
                    }
                }
            }
            group?.leave()
        })
    }
    
    /// Take user to sign-in screen. If opening is present, let it take us there, otherwise replace rootVC.
    static func logout(ispatchedInGroup group: DispatchGroup?, animated: Bool) {
        DispatchQueue.main.async(execute: {
            // Remove token
            _ = removeToken()
            // Clear preferences
            UserPreferences.clearPreferences()
            // Present resulting VC
            if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
                if window.rootViewController?.presentedViewController as? OpeningViewController == nil,
                    let registration = UIStoryboard(name: "SignUp", bundle: nil).instantiateInitialViewController() as? HomeTabBarViewController {
                    window.rootViewController?.presentedViewController?.present(registration, animated: animated, completion: nil)
                }
            }
            group?.leave()
        })
    }
    
    /// Take user to the opening scene.
    static func presentOpening() {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window,
            let opening = UIStoryboard(name: "Opening", bundle: nil).instantiateInitialViewController() as? OpeningViewController {
            window.rootViewController = opening
        }
    }
    
}
