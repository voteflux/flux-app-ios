//
//  FeedbackViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 7/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var feedbackTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor
        
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        // Feedback
        feedbackTextView.delegate = self
        feedbackTextView.layer.cornerRadius = 5
        feedbackTextView.layer.borderColor = colors.highlightColor.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func send() {
        if let feedback = feedbackTextView.text {
            LoadingIndicator.standard.show(onView: view)
            FluxAPI.send(feedback: feedback, onSuccess: onSuccess, onFail: onFail)
        }
    }

    func onSuccess() {
        LoadingIndicator.standard.hide()
        feedbackTextView.text = ""
    }
    
    /// Presents error to user with message
    func onFail(message: String?) {
        LoadingIndicator.standard.hide()
        self.presentError(title: errorTitle, message: message, close: nil)
    }
    
    // MARK: UITextViewDelegate
    
    @objc func hideKeyboard() {
        feedbackTextView.resignFirstResponder()
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    
}
