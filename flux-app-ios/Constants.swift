//
//  Constants.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation
import UIKit

let fluxHostname = "https://api.voteflux.org" // "https://flux-api-dev.herokuapp.com"
let addressVersion = 1

extension UIViewController {
    func presentError(title: String?, message: String?, close: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: close, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

/// Error title shown to user
let errorTitle: String? = "There was a problem"
/// Dismiss button for an error shown to the user
let errorClose: String? = "Okay"
/// Default error message.
let errorMessage: String? = nil


// MARK: FluxAPI

/// String length of generated nonce.
var nonceLength = 32

var success = "🎉"

/// Stores index keys used with the keychain
struct KeychainKeys {
    static let s: String = "s"
}

enum CustomiseMembershipKeys: String {
    case Volunteer = "volunteer"
    enum Candidate: String {    // Make these all the same when posting
        case Federal = "candidature_federal"
        case State = "candidature_state"
        case Local = "candidature_local"
    }
    case Organise = "vol_organise"
    case Member = "state_consent"
    
    case Token = "s"
}

enum RegistrationKeys: String {
    case FirstName = "fname"
    case MiddleNames = "mnames"
    case Surname = "sname"
    case Email = "email"
    case Name = "name"
    
    case PhoneNumber = "contact_number"
    case Nonce = "verify_nonce"
    case SMSCode = "verify_code"
    
    case Postcode = "addr_postcode"
    case Suburb = "addr_suburb"
    case Country = "addr_country"
    case StreetName = "addr_street"
    case StreetNumber = "addr_street_no"
    
    case Address = "address"
    
    case DOBYear = "dobDay"
    case DOBMonth = "dobMonth"
    case DOBDay = "dobYear"
    
    case DOB = "dob"
    
    case IsRegistered = "onAECRoll"
    
    case AddressVersion = "addr_version"
}

enum PhoneVerificationKeys: String {
    
    case PhoneNumber = "phone_number"
    case Nonce = "verify_nonce"
    case SMSCode = "verify_code"
    
}

enum MagicLinkKeys: String {
    case Email = "email"
}

enum FeedbackKeys: String {
    case token = "s"
    case feedback = "feedback"
}
