//
//  RegistrationNavigationController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 8/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class RegistrationNavigationController: UINavigationController, RegistrationProtocol {

    // MARK: Registration
    
    var form: RegistrationForm = RegistrationForm()
    
    // MARK: Properties
    
    let addressVersion: String = "1.0"
    
    /// Previously verified phone number
    private var _verifiedPhone: TelephoneNumber? = nil
    
    /// Randomly generated token used to verify phone
    private var _verifiedNonce: String? = nil
    
    /// Current phone number
    var phone: TelephoneNumber? = nil
    
    /// Randomly generated token
    var nonce: String? = nil
    
    // MARK: Personal details
    
    var firstName: String? = nil
    var middleNames: String? = nil
    var lastName: String? = nil
    
    var email: String? = nil
    
    // MARK: Address details
    
    var postcode: String? = nil
    var suburb: String? = nil
    var country: String? = "au"
    var streetName: String? = nil
    var streetNumber: String? = nil
    
    var DOBYear: String? = nil
    var DOBMonth: String? = nil
    var DOBDay: String? = nil
    
    var isRegistered: String? = nil
   
    
    
    /// Previously verified phone number (excluding the country code)
    private var _verifiedPhoneNumber: String? = nil
    /// Perviously verified
    private var _verifiedPhoneCountryCode: String? = nil
    
    var _phone: String? = nil
    
    var phoneCountryCode: String? = nil
    var phoneNumber: String? = nil
    
    
    
    // MARK: Methods
    
    /// Stored phone is the verified phone
    func phoneIsVerified() -> Bool {
        return phone == _verifiedPhone && phone != nil
    }
    
    /// Set verified phone. Phone and nonce will be either both be defined or both undefined.
    func setVerified(phone: TelephoneNumber?, nonce: String?) {
        if let phone = phone, let nonce = nonce {
            _verifiedPhone = phone
            _verifiedNonce = nonce
        } else {
            _verifiedPhone = nil
            _verifiedNonce = nil
        }
    }
    
    func isReadyToRegister() -> Bool {
        return postcode != "" && postcode != nil &&
                streetName != "" && streetName != nil &&
                streetNumber != "" && streetNumber != nil &&
                isRegistered != "" && isRegistered != nil
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
