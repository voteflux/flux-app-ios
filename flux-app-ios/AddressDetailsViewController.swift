//
//  AddressDetailsViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class AddressDetailsViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, RegistrationPageProtocol {

    // MARK: Properties
    
    var suburbPickerView: UIPickerView!
    var streetNamePickerView: UIPickerView!
    var datePickerView: UIDatePicker!
    
    // MARK: IBProperties
    
    @IBOutlet weak var postcodeField: UITextField!
    @IBOutlet weak var suburbField: UITextField!
    @IBOutlet weak var streetNumberField: UITextField!
    @IBOutlet weak var streetNameField: UITextField!
    
    @IBOutlet weak var dayField: UITextField!
    @IBOutlet weak var monthField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    
    @IBOutlet weak var isRegisteredVoterButton: UIButton!
    
    /// Clickable region to open DOB picker
    @IBOutlet weak var dobView: UIView!
    
    /// Hidden DOB field
    @IBOutlet weak var dobHiddenField: UITextField!
    
    // MARK: - Preparation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Background
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        view.backgroundColor = colors.backgroundColor
        
        // Postcode
        postcodeField.delegate = self
        postcodeField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Suburb
        suburbField.delegate = self
        suburbPickerView = UIPickerView()
        suburbPickerView.delegate = self
        suburbField.inputView = suburbPickerView
        
        // Street number
        streetNumberField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Street name
        streetNameField.delegate = self
        streetNamePickerView = UIPickerView()
        streetNamePickerView.delegate = self
        streetNameField.inputView = streetNamePickerView
        
        // Date of birth
        let openDOBGesture = UITapGestureRecognizer(target: self, action: #selector(editDOB))
        dobView.addGestureRecognizer(openDOBGesture)
        datePickerView = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date.init()
        dobHiddenField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        
        dayField.delegate = self
        dayField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        monthField.delegate = self
        monthField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        yearField.delegate = self
        yearField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // IsRegistered
        isRegisteredVoterButton.isSelected = false
        isRegisteredVoterButton.setImage(UIImage(named: "CheckboxOn"), for: .selected)
        isRegisteredVoterButton.setImage(UIImage(named: "CheckboxOff"), for: .normal)
    }
    
    /// Populate fields
    override func viewWillAppear(_ animated: Bool) {
        postcodeField.text = registration.postcode
        suburbField.text = registration.suburb
        streetNameField.text = registration.streetName
        streetNumberField.text = registration.streetNumber
        dayField.text = registration.DOBDay
        monthField.text = registration.DOBMonth
        yearField.text = registration.DOBYear
        isRegisteredVoterButton.isSelected = registration.isRegistered ?? false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Registration
    
    @IBAction func finish() {
        registration.isRegistered = isRegisteredVoterButton.isSelected
        register()
    }
    
    /// If client-side requirements are met, try registering the user.
    func register() {
        if registration.isReadyToRegister() {
            LoadingIndicator.standard.show(onView: view)
            FluxAPI.registerAllAtOnce(form: registration.asParameters, onSuccess: onSuccessRegister, onFail: onFail)
        }
    }
    
    /// Receives server response containing s and user.
    func onSuccessRegister(json: NSDictionary) {
        LoadingIndicator.standard.hide()
        
        // Store secret token
        if let s = json["s"] as? String {
            _ = updateToken(s)
        }
        
        // Set default preferences
        if let preferences = json["user"] as? NSDictionary {
            UserPreferences.storeUserPreferences(preferences: preferences)
        }
        
        // Segue
        let window = (UIApplication.shared.delegate as? AppDelegate)?.window
        let customise = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CustomiseMembership")
        window?.rootViewController = customise
    }
    
    /// Queries suburbs on postcode
    func onSuccessSuburbs(suburbs: [String]) {
        suburbOptions = suburbs
        suburbField.becomeFirstResponder()
        postcodeField.resignFirstResponder()
        LoadingIndicator.standard.hide()
    }
    
    /// Queries for streets on suburb and postcode
    func onSuccessStreets(streets: [String]) {
        streetNameOptions = streets
        streetNameField.becomeFirstResponder()
        suburbField.resignFirstResponder()
        LoadingIndicator.standard.hide()
    }
    
    /// Hide loading indicator and present an error.
    func onFail(message: String?) {
        LoadingIndicator.standard.hide()
        self.presentError(title: errorTitle, message: message, close: errorClose)
    }
    
    // MARK: Checkboxes
    
    /// Toggle the isRegistered
    @IBAction func toggleButton(_ sender: UIButton) {
        hideKeyboard()
        sender.isSelected = !sender.isSelected
    }
    
    // MARK: UITextFieldDelegate
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    /// Maximum length of a postcode.
    let postcodeLength = 4
    
    /// Prevent pasting/typing (some types of) invalid text into fields.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField === postcodeField {
            // Limit to postcodeLength
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            
            // Clear suburb and street name
            suburbField.text = nil
            streetNameField.text = nil
            registration.suburb = nil
            registration.streetName = nil
            
            return newLength <= postcodeLength
        } else if textField === suburbField {
            return false
        } else if textField === streetNameField {
            return false
        }
        return true
    }
    
    /// Updates registration form and other dependent fields on textfield change.
    func textFieldDidChange(_ textField: UITextField) {
        if textField === postcodeField, let postcode = textField.text, postcode.characters.count == postcodeLength {
            // Update postcode field, clear suburb & street
            LoadingIndicator.standard.show(onView: view)
            
            registration.postcode = postcode
            registration.streetName = nil
            registration.suburb = nil
            
            suburbField.text = nil
            suburbOptions = []
            streetNameField.text = nil
            streetNameOptions = []
            
            FluxAPI.querySuburbs(country: "au", postcode: postcode, onSuccess: onSuccessSuburbs, onFail: onFail)
        } else if textField === suburbField, let suburb = textField.text, suburb != "" {
            // Update suburb field, clear street
            LoadingIndicator.standard.show(onView: view)
            
            registration.suburb = suburb
            registration.streetName = nil
            
            streetNameField.text = nil
            streetNameOptions = []
            
            FluxAPI.queryStreets(country: "au", postcode: registration.postcode!, suburb: registration.suburb!, onSuccess: onSuccessStreets, onFail: onFail)
        } else if textField === streetNameField, let streetName = textField.text, streetName != "" {
            registration.streetName = streetName
        } else if textField == streetNumberField {
            registration.streetNumber = streetNumberField.text
        }
    }
    
    // MARK: UIPickerView
    
    /// Start editing the DOB field
    @objc func editDOB() {
        dobHiddenField.becomeFirstResponder()
    }
    
    /// Sets DD/MM/YYYY fields from selected date.
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let date = sender.date
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        registration.DOBDay = String(format: "%02d", day)
        registration.DOBMonth = String(format: "%02d", month)
        registration.DOBYear = String(format: "%04d", year)
        
        dayField.text = String(format: "%02d", day)
        monthField.text = String(format: "%02d", month)
        yearField.text = String(format: "%04d", year)
    }
    
    /// Picker options available for suburbs
    var suburbOptions: [String] = [""] {
        didSet(newSuburbs) {
            suburbPickerView.reloadAllComponents()
        }
    }
    
    /// Picker options available for street names
    var streetNameOptions: [String] = [""] {
        didSet(newStreetNames) {
            streetNamePickerView.reloadAllComponents()
        }
    }
    
    /// Show suburbs and street names in single columns
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView === suburbPickerView {
            return suburbOptions.count
        } else if pickerView === streetNamePickerView {
            return streetNameOptions.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView === suburbPickerView {
            return suburbOptions[row]
        } else if pickerView === streetNamePickerView {
            return streetNameOptions[row]
        } else {
            return nil
        }
    }
    
    /// Set textfield when row is selected
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView === suburbPickerView {
            //
            if row < suburbOptions.count {
                let suburb = suburbOptions[row]
                suburbField.text = suburb
                textFieldDidChange(suburbField)
            }
        } else if pickerView === streetNamePickerView {
            //
            if row < streetNameOptions.count {
                let streetName = streetNameOptions[row]
                streetNameField.text = streetName
                textFieldDidChange(streetNameField)
            }
        }
    }
    
    // MARK: - RegistrationPageProtocol
    
    var registration: RegistrationForm {
        get {
            return (navigationController as? RegistrationNavigationController)!.form
        }
        set {
            (navigationController as? RegistrationNavigationController)!.form = newValue
        }
    }
    
    /// All required field are filled
    func meetsRequirements() -> Bool {
        return registration.isReadyToRegister()
    }
    
}
