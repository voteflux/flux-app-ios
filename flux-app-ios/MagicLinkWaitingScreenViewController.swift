//
//  MagicLinkWaitingScreenViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 6/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class MagicLinkWaitingScreenViewController: UIViewController {

    @IBOutlet weak var sentEmailToField: UILabel!
    
    var email: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor
        
        // Do any additional setup after loading the view.
        if let email = email {
            sentEmailToField.text = "We sent an email to\n\(email)"
            sentEmailToField.boldRange(NSMakeRange(20, sentEmailToField.text!.characters.count - 20))
        } else {
            sentEmailToField.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onSuccess(_ s: String) {
        _ = updateToken(s)
    }
    
    /// Presents error with message to the user.
    func onFail(_ message: String?) {
        self.presentError(title: errorTitle, message: message, close: nil)
    }

}
