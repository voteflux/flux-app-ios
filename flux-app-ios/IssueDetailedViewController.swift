//
//  IssueDetailedViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 9/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

class IssueDetailedViewController: UIViewController {
    
    @IBOutlet weak var detailsTextView: UITextView!
    
    @IBOutlet weak var affirmButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    var issue: Issue? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        
        // Style issue title
        if let nav = self.navigationController {
            nav.isNavigationBarHidden = false
            
            let navigationBar = nav.navigationBar
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.isTranslucent = true
            navigationBar.shadowImage = UIImage()
            nav.setNavigationBarHidden(false, animated:true)
            
            navigationBar.tintColor = styles.highlightColor
            
            let attributedTitle = NSMutableAttributedString(
                string: issue?.title ?? "",
                attributes: [
                    NSFontAttributeName: UIFont(name: "Lato-Light", size: 24.0)!,
                ])
            
            let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
            titleLabel.attributedText = attributedTitle
            titleLabel.backgroundColor = UIColor.clear
            titleLabel.textColor = colors.titleColor
            titleLabel.textAlignment = .center
            
            nav.navigationBar.topItem?.titleView = titleLabel
        }
        
        // Style feedback field
        detailsTextView.layer.cornerRadius = 5
        
        // Style button
        affirmButton.layer.cornerRadius = 5
        affirmButton.layer.borderWidth = 2
        affirmButton.layer.borderColor = colors.highlightColor.cgColor
        affirmButton.backgroundColor = UIColor.clear
        affirmButton.imageView?.image = UIImage(named: "Tick")?.imageWithColor(color: colors.highlightColor)
        
        rejectButton.layer.cornerRadius = 5
        rejectButton.layer.borderWidth = 2
        rejectButton.layer.borderColor = colors.highlightColor.cgColor
        rejectButton.backgroundColor = UIColor.clear
        rejectButton.imageView?.image = UIImage(named: "Cross")?.imageWithColor(color: colors.highlightColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        detailsTextView.text = issue?.description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Voting
    
    /// Casts vote on issue
    func vote(choice: Bool) {
        if let voteVC = navigationController?.viewControllers[0] as? VoteViewController, let issue = issue {
            voteVC.vote(onIssue: issue, choice: choice)
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    /// Votes yes on issue and removes from stack
    @IBAction func affirmativeVote() {
        vote(choice: true)
    }

    /// Votes no on issue and removes from the stack
    @IBAction func negativeVote() {
        vote(choice: false)
    }
    
}
