//
//  Issue.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 9/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

struct Issue {
    
    let title: String
    
    let description: String
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
    
}
