//
//  RegistrationForm.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 10/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

struct RegistrationForm {
    
    let addressVersion: String = "1.0"
    
    /// Previously verified phone number
    private var _verifiedPhone: TelephoneNumber? = nil
    
    /// Randomly generated token used to verify phone
    private var _verifiedNonce: String? = nil
    
    /// Current phone number
    var phone: TelephoneNumber? = nil {
        didSet {
            if phone == _verifiedPhone {
                nonce = _verifiedNonce
            }
        }
    }
    
    /// Randomly generated token
    var nonce: String? = nil
    
    // MARK: Personal details
    
    var firstName: String? = nil
    var middleNames: String? = nil
    var lastName: String? = nil
    
    var email: String? = nil
    
    // MARK: Address details
    
    var postcode: String? = nil
    var suburb: String? = nil
    var country: String? = "Australia"
    var streetName: String? = nil
    var streetNumber: String? = nil
    
    var DOBYear: String? = nil
    var DOBMonth: String? = nil
    var DOBDay: String? = nil
    
    var isRegistered: Bool? = nil
    
    // MARK: Methods
    
    /// Stored phone is the verified phone
    func phoneIsVerified() -> Bool {
        return phone == _verifiedPhone && phone != nil
    }
    
    /// Set verified phone. Phone and nonce will be either both be defined or both undefined.
    mutating func setVerified(phone: TelephoneNumber?, nonce: String?) {
        if let phone = phone, let nonce = nonce {
            _verifiedPhone = phone
            _verifiedNonce = nonce
        } else {
            _verifiedPhone = nil
            _verifiedNonce = nil
        }
    }
    
    /// Registration form meets client-side checks
    func isReadyToRegister() -> Bool {
        return postcode != "" && postcode != nil &&
            streetName != "" && streetName != nil &&
            streetNumber != "" && streetNumber != nil &&
            isRegistered != nil
    }
    
    /// Make these parameters into a dictionary
    var asParameters: [String: AnyObject] {
        var parameters = [String: AnyObject]()
        
        parameters[RegistrationKeys.AddressVersion.rawValue] = addressVersion as AnyObject
        
        parameters[RegistrationKeys.PhoneNumber.rawValue] = _verifiedPhone?.internationalNumber as AnyObject
        parameters[RegistrationKeys.Nonce.rawValue] = _verifiedNonce as AnyObject
        
        parameters[RegistrationKeys.FirstName.rawValue] = firstName as AnyObject
        parameters[RegistrationKeys.MiddleNames.rawValue] = middleNames as AnyObject
        parameters[RegistrationKeys.Surname.rawValue] = lastName as AnyObject
        parameters[RegistrationKeys.Email.rawValue] = email as AnyObject
        parameters[RegistrationKeys.Country.rawValue] = country as AnyObject
        
        parameters[RegistrationKeys.Postcode.rawValue] = postcode as AnyObject
        parameters[RegistrationKeys.Suburb.rawValue] = suburb as AnyObject
        parameters[RegistrationKeys.StreetName.rawValue] = streetName as AnyObject
        parameters[RegistrationKeys.StreetNumber.rawValue] = streetNumber as AnyObject
        
        parameters[RegistrationKeys.DOBDay.rawValue] = DOBDay as AnyObject
        parameters[RegistrationKeys.DOBMonth.rawValue] = DOBMonth as AnyObject
        parameters[RegistrationKeys.DOBYear.rawValue] = DOBYear as AnyObject
        
        parameters[RegistrationKeys.IsRegistered.rawValue] = isRegistered as AnyObject
        
        parameters[RegistrationKeys.Address.rawValue] = String(
            format: "%@; %@; %@; %@; %@", arguments: [
            streetNumber!, streetName!, suburb!, postcode!, country!]) as AnyObject
        parameters[RegistrationKeys.Name.rawValue] = String(
            format: "%@ %@ %@",
            arguments: [firstName!, middleNames!, lastName!]) as AnyObject
        parameters[RegistrationKeys.DOB.rawValue] = String(format: "%@-%@-%@T00:00:00.000Z", arguments: [DOBYear!, DOBMonth!, DOBDay!]) as AnyObject
        
        return parameters
    }
    
}
