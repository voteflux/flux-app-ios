//
//  VoteViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 7/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class VoteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate {

    let issueDetailsSegue: String = "issueDetails"
    
    @IBOutlet weak var outOfIssuesLabel: UILabel!
    @IBOutlet weak var issuesTableView: UITableView!
    
    let expansionSettings: MGSwipeExpansionSettings = .init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor

        // Do any additional setup after loading the view.
        issuesTableView.delegate = self
        issuesTableView.dataSource = self
        
        issuesTableView.layer.cornerRadius = 5
        
        // MGSwipeTable customisation
        expansionSettings.fillOnTrigger = true
        expansionSettings.buttonIndex = 0
        expansionSettings.threshold = 2.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        if issues.count == 0 {
            issuesTableView.isHidden = true
            outOfIssuesLabel.isHidden = false
        } else {
            issuesTableView.isHidden = false
            outOfIssuesLabel.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Business Logic
    
    func vote(onIssue issue: Issue, choice: Bool) {
        // Find indexPath.row
        if let i = issues.index(where: { $0.title == issue.title }) {
            issues.remove(at: i)
            issuesTableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.fade)
        }
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let issueVC = segue.destination as? IssueDetailedViewController, let issue = sender as? Issue {
            issueVC.issue = issue
        }
    }

    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: issueDetailsSegue, sender: issues[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    // MARK: MGSwipeTableCellDelegate
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
        return true
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        debugPrint(index)
        
        // Find index from title
        if let cellTitle = cell.textLabel?.text, let issue = issues.filter({ $0.title == cellTitle }).first {
            let choice = (direction == .leftToRight) ? true : false
            vote(onIssue: issue, choice: choice)
        }
        
        return true
    }
    
    // MARK: UITableViewDataSource
    
    let issueReuseIdentifier: String = "Issue"
    
    var issues: [Issue] = [
        Issue(title: "Gay Marriage", description: "Gay marriage, also known as same-sex marriage, is the marriage between two people of the same sex. The Australian government outlawed same-sex marriage when it passed the Marriage Act of 1961. The act stated that the government would not recognise same-sex marriages, traditional Aboriginal marriages or polygamous marriages. In 2016 the leaders of Australia’s major political parties voiced their support for legalizing same-sex marriage though a formal bill has yet to be introduced."),
        Issue(title: "Negative Gearing", description: "Negative gearing is the practice of using losses on property investments to reduce taxable income. In 2013, approximately 1.3 million Australians used the concession. Data shows that high income earners write off much larger percentages of their taxes than those who earn lower wages. In 2012, surgeons wrote off $4,161 of their taxes using negative gearing while teachers wrote off $327. Proponents, including Malcolm Turnbull, argue that the practice has been part of Australian tax law since 1915 and is not a tax break since the real estate investor is taking a loss to their assets. Opponents argue that the policy disproportionately benefits Australians in high-paying occupations, not those of average incomes, since they are much more likely to own investment properties. "),
        Issue(title: "Rent Tax", description: "The Minerals Resource Rent Tax came into effect on July 1, 2012. It is a 22.5 per cent tax on the profits of iron ore and coal projects but only applies to profits over $75 million. There have been calls on different sides to both abolish and expand the tax."),
        Issue(title: "Internet Regulation", description: "The Australian Communications and Media Authority (ACMA) has the power to enforce content restrictions on Internet content hosted within Australia, and maintain a \"black-list\" of overseas websites which is then provided for use in filtering software. The restrictions focus primarily on child pornography, sexual violence, and other illegal activities, compiled as a result of a consumer complaints process. In 2009, the OpenNet Initiative found no evidence of Internet filtering in Australia, but due to legal restrictions ONI does not test for filtering of child pornography. ")] {
        didSet {
            if issues.count == 0 {
                issuesTableView.isHidden = true
                outOfIssuesLabel.isHidden = false
            } else {
                issuesTableView.isHidden = false
                outOfIssuesLabel.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: issueReuseIdentifier) as! MGSwipeTableCell!
        if cell == nil
        {
            cell = MGSwipeTableCell(style: UITableViewCellStyle.default, reuseIdentifier: issueReuseIdentifier)
        }
        
        cell?.textLabel!.text = issues[indexPath.row].title
        cell?.delegate = self //optional
        
        cell?.selectionStyle = .none
        cell?.leftExpansion = expansionSettings
        cell?.rightExpansion = expansionSettings
        
        cell?.backgroundColor?.withAlphaComponent(0.9)
        cell?.swipeContentView.backgroundColor?.withAlphaComponent(0.9)
        cell?.swipeBackgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.9)
        
        //configure left buttons
        cell?.leftButtons = [MGSwipeButton(title: "", icon: UIImage(named: "Tick"), backgroundColor: UIColor(red: 64/255, green: 227/255, blue: 29/255, alpha: 1.0))]
        cell?.leftSwipeSettings.transition = MGSwipeTransition.clipCenter
        
        //configure right buttons
        cell?.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named: "Cross"), backgroundColor: UIColor(red: 227/255, green: 53/255, blue: 9/255, alpha: 1.0))]
        cell?.rightSwipeSettings.transition = MGSwipeTransition.clipCenter
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issues.count
    }
    
}
