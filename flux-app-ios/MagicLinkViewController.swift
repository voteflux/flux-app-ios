//
//  MagicLinkViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class MagicLinkViewController: UIViewController, UITextFieldDelegate {
    
    // MARK:  Segues
    
    let magicLinkRequestedSegue = "magicLinkRequested"
   
    // MARK: Outlets
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colors.backgroundColor

        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        emailField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func sendLink() {
        // TODO: Client-side checking
        
        LoadingIndicator.standard.show(onView: view)
        FluxAPI.requestMagicLink(toEmail: emailField.text ?? "", onSuccess: onSuccess, onFail: onFail)
    }
    
    func onSuccess(withEmail email: String) {
        LoadingIndicator.standard.hide()
        performSegue(withIdentifier: magicLinkRequestedSegue, sender: nil)
    }
    
    func onFail(withMessage message: String?) {
        LoadingIndicator.standard.hide()
        self.presentError(title: errorTitle, message: message, close: nil)
    }
    
    // MARK: UITextFieldDelegate
    
    @objc func hideKeyboard() {
        emailField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailField.resignFirstResponder()
        sendLink()
        return true
    }

}
