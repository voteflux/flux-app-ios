//
//  PickerOnlyTextField.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 2/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class PickerOnlyTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste) {
            return false
        } else if action == #selector(replace) {
            return false
        } else if action == #selector(cut) {
            return false
        } else {
            return true
        }
    }

}
