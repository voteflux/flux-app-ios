//
//  OpeningViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 6/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

/// Whether the opening videos(s) have beeen 
var openingPlayed = false

class OpeningViewController: UIViewController {
    
    let welcomeVideoName = "02_Word_Opener"
    let logoVideoName = "01_Flux Logo_Screen_Transition"
    
    var welcomeObserver: Any! = nil
    var logoObserver: Any! = nil
    
    var player = AVPlayer()
    var layer: AVPlayerLayer! = nil
    
    let signInSegue = "SignUp"
    let homeSegue = "Home"
    let customiseMembershipSegue = "CustomiseMembership"
    
    
    var magicDispatchGroup: DispatchGroup? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = colors.backgroundColor

        // These are stored in keychain, so, if reinstalling remove.
        if !hasAppAlreadyLaunchedOnce() {
            _ = removeToken()
            setCustomisedBefore(false)
        }
        
        // Prepare AVPlayer
        
        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEnteredBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        do {
            // Mute audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch {
            
        }
        layer = AVPlayerLayer(player: player)
        layer.frame = self.view.frame
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(layer)
        layer.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Play welcome screen video
        if openingPlayed {
            videosDidFinish()
        } else if !hasAppAlreadyLaunchedOnce() {
            playWelcomeVideo()
        } else {
            playLogoVideo()
        }
    }
    
    /// removes observer watching VC in and out of background
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func appEnteredBackground() {
        player.pause()
    }
    
    @objc func appEnteredForeground() {
        if openingPlayed {
            videosDidFinish()
        } else if !hasAppAlreadyLaunchedOnce() {
            playWelcomeVideo()
        } else {
            playLogoVideo()
        }
    }
    
    /// Makes an asynchronous call which plays vide on completion.
    private func play(video item: AVPlayerItem, _ asset: AVAsset) {
        openingPlayed = true
        asset.loadValuesAsynchronously(forKeys: ["playable", "duration"]) {
            var error: NSError? = nil
            let status = asset.statusOfValue(forKey: "duration", error: &error)
            switch status {
                case .loaded:
                    DispatchQueue.main.async {
                        self.player.replaceCurrentItem(with: item)
                        self.layer.isHidden = false
                        self.player.isMuted = true
                        self.player.play()
                    }
                default:
                    return
            }
        }
    }
    
    /// Loads welcome video, makes call to play it.
    private func playWelcomeVideo() {
        guard let path = Bundle.main.path(forResource: welcomeVideoName, ofType: "mp4") else {
            debugPrint("\(welcomeVideoName) not found.")
            return
        }
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        let item = AVPlayerItem(asset: asset)
        
        // Play next video
        welcomeObserver = NotificationCenter.default.addObserver(self, selector: #selector(welcomeVideoDidFinish),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        play(video: item, asset)
    }
    
    /// Loads logo video, makes call to play it.
    private func playLogoVideo() {
        guard let path = Bundle.main.path(forResource: logoVideoName, ofType: "mp4") else {
            debugPrint("\(logoVideoName) not found.")
            return
        }
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        let item = AVPlayerItem(asset: asset)
        
        // Finished
        logoObserver = NotificationCenter.default.addObserver(self, selector: #selector(logoVideoDidFinish),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        play(video: item, asset)
    }
    
    /// Removes observer, sets app has launched and plays logo video
    @objc func welcomeVideoDidFinish() {
        NotificationCenter.default.removeObserver(welcomeObserver)
        setHasAppAlreadyLaunchedOnce(true)
        playLogoVideo()
    }
    
    /// Removes observer, all videos have finished.
    @objc func logoVideoDidFinish() {
        NotificationCenter.default.removeObserver(logoObserver)
        videosDidFinish()
    }
    
    /// Handles flow once videos are completed.
    func videosDidFinish() {
        if let magicDispatchGroup = magicDispatchGroup {
            DispatchQueue.global().async {
                _ = magicDispatchGroup.wait(timeout: DispatchTime(uptimeNanoseconds: 2000000000))
                DispatchQueue.main.async {
                    self.continueApp()
                }
            }
        } else {
            continueApp()
        }
    }
    
    func continueApp() {
        // Perform correct segue
        if retrieveToken() != nil {
            if hasCustomisedBefore() {
                // Dashboard
                performSegue(withIdentifier: homeSegue, sender: nil)
            } else {
                // Customise screen
                performSegue(withIdentifier: customiseMembershipSegue, sender: nil)
            }
        } else {
            // Sign-In screen
            performSegue(withIdentifier: signInSegue, sender: nil)
        }
        
        // Clean-up
        player.replaceCurrentItem(with: nil)
        player.pause()
    }

}
