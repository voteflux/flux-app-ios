//
//  Boldify.h
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 6/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)
- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;
@end
