//
//  TelephoneNumber.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 8/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

struct TelephoneNumber {
    let countryCode: String
    let subscriberNumber: String
    
    var internationalNumber: String {
        get {
            return countryCode + subscriberNumber
        }
    }
    
    init (countryCode: String, subscriberNumber: String) {
        self.countryCode = countryCode
        self.subscriberNumber = subscriberNumber
    }
}

// MARK: Equatable

func ==(lhs: TelephoneNumber?, rhs: TelephoneNumber?) -> Bool {
    return lhs?.countryCode == rhs?.countryCode && lhs?.subscriberNumber == rhs?.subscriberNumber
}
