//
//  AppDelegate.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        return true
    }
    
    /// Asks the delegate to open a resource identified by a URL.
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let components = url.pathComponents
        if let host = url.host {
            switch host {
                case "api.voteflux.org", "flux-api-dev.herokuapp.com":
                    if components.count >= 4 {
                        switch (components[0], components[1], components[2]) {
                        case ("/", "sign_in", "magic_link"):
                            return true
                        default:
                            return false
                        }
                    }
                default:
                    return false
            }
        }
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let components = url.pathComponents
        if let host = url.host {
            switch host {
            case "api.voteflux.org", "flux-api-dev.herokuapp.com":
                if components.count >= 4 {
                    switch (components[0], components[1], components[2]) {
                    case ("/", "sign_in", "magic_link"):
                        /**
                         * If app is opened...
                         *      --> Customise, setting token
                         * If app is closed...
                         *      --> Opening --> Customise
                         */
                        return true
                    default:
                        return false
                    }
                }
            default:
                return false
            }
        }
        return false
        
    }
    
    func handleURL(_ url: URL) -> Bool {
        let components = url.pathComponents
        if let host = url.host {
            switch host {
            case "api.voteflux.org", "flux-api-dev.herokuapp.com":
                if components.count >= 4 {
                    switch (components[0], components[1], components[2]) {
                    case ("/", "sign_in", "magic_link"):
                        // Make an sync request to validate...
                        //      make the opening controller wait for response
                        let group = DispatchGroup()
                        group.enter()
                        // Initialise async- login request.
                        FluxAPI.login(
                            withMagicToken: components[3],
                            onFail: nil,
                            dispatchGroup: group)
                        // Present opening view controller
                        Session.presentOpening()
                        return true
                    default:
                        return false
                    }
                }
            default:
                return false
            }
        }
        return false
    }
    
    /// Tells the delegate that the data for continuing an activity is available.
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        //
        if let url = userActivity.webpageURL, userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            return handleURL(url)
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

