//
//  HomeTabBarViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 7/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tabBar.barTintColor = styles.highlightColor
        tabBar.tintColor = UIColor.white
        
        tabBar.unselectedItemTintColor = UIColor(red: 50, green: 50, blue: 50, alpha: 0.5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
