//
//  File.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 10/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import Foundation

protocol RegistrationPageProtocol {
    
    var registration: RegistrationForm { get set }
 
    func meetsRequirements() -> Bool
    
}
