//
//  LoginViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var memberButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = colors.backgroundColor
        
        memberButton.layer.borderWidth = 1
        memberButton.layer.cornerRadius = 5
        memberButton.layer.borderColor = styles.highlightColor.cgColor
        
        // Style text
        descriptionLabel.text = "The most advanced political movement ever.\n Free and Secure."
        descriptionLabel.boldRange(NSMakeRange(9, 8))
        descriptionLabel.boldRange(NSMakeRange(44, 4))
        descriptionLabel.boldRange(NSMakeRange(53, 6))
        
        if let nav = self.navigationController {
            let navigationBar = nav.navigationBar
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.isTranslucent = true
            navigationBar.shadowImage = UIImage()
            nav.setNavigationBarHidden(false, animated:true)
            
            navigationBar.tintColor = styles.highlightColor
            nav.navigationItem.titleView?.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        let fontSize = UIFont.systemFontSize
        let attrs = [
            NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
            NSForegroundColorAttributeName: UIColor.black
        ]
        let nonBoldAttribute = [
            NSFontAttributeName: UIFont.systemFont(ofSize: fontSize),
            ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
