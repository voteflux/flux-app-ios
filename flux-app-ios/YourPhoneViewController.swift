//
//  YourPhoneViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit
import Alamofire

class YourPhoneViewController: UIViewController, UITextFieldDelegate, RegistrationPageProtocol {
    
    // MARK: Constants

    private let verificationCodeSentSegue = "verificationCodeSent"
    private let phoneUnchangedSegue = "phoneUnchanged"
    
    // MARK: RegistrationPageProtocol
    
    var registration: RegistrationForm {
        get {
            return (navigationController as? RegistrationNavigationController)!.form
        }
        set {
            (navigationController as? RegistrationNavigationController)!.form = newValue
        }
    }
    
    func meetsRequirements() -> Bool {
        return registration.phone != nil
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var phoneNumberField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isNewMember = true
        
        // Do any additional setup after loading the view.
        style()
        
        view.backgroundColor = colors.backgroundColor
        
        self.navigationController?.navigationItem.titleView?.isHidden = true
        
        phoneNumberField.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        phoneNumberField.delegate = self;
        phoneNumberField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Style sendButton
        sendButton.contentHorizontalAlignment = .center
        sendButton.contentVerticalAlignment = .center
        
        // Open keyboard
        //phoneNumberField.becomeFirstResponder()
    }
    
    /// Update fields using registration progress
    override func viewWillAppear(_ animated: Bool) {
        // Load fields
        if let subscriberNumber = registration.phone?.subscriberNumber {
            phoneNumberField.text = subscriberNumber
        }
        
        // Set button text
        if registration.phoneIsVerified() {
            sendButton.setTitle("Next", for: .normal)
        } else {
            sendButton.setTitle("Send Verification Code", for: .normal)
        }
    }
    
    // MARK: Styling
    
    @IBOutlet weak var topRow: UIView!
    
    @IBOutlet weak var constraintCountryButtonRightPadding: NSLayoutConstraint!
    @IBOutlet weak var constraintCountryTextPaddingButton: NSLayoutConstraint!
    @IBOutlet weak var constraintCountryTextPaddingLeft: NSLayoutConstraint!
    
    /// Add styling
    func style() {
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    /// Check whether required fields have been captured
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // Phone verification form should have phone number
        return registration.phone != nil
    }
    
    /// Sends verification to set number.
    @IBAction func sendVerification() {
        if let phone = registration.phone {
            if registration.phoneIsVerified() {
                // No change to phone
                performSegue(withIdentifier: phoneUnchangedSegue, sender: nil)
            } else {
                // Verify this new number
                LoadingIndicator.standard.show(onView: view)
                FluxAPI.requestCommitPhone(phone: phone, onSuccess: onSuccess, onFail: onFail)
            }
        }
    }
    
    /// If phone number still matches, continue to next page.
    func onSuccess(phone: TelephoneNumber) {
        LoadingIndicator.standard.hide()
        if phone == registration.phone {
            performSegue(withIdentifier: verificationCodeSentSegue, sender: nil)
        }
    }
    
    /// Present alert error alert with message
    func onFail(message: String?) {
        LoadingIndicator.standard.hide()
        self.presentError(title: errorTitle, message: message, close: errorClose)
    }
    
    // MARK: UITextFieldDelegate
    
    @objc func hideKeyboard() {
        phoneNumberField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    /// Updates stored phone and button text.
    func textFieldDidChange(_ textField: UITextField) {
        // Update phone number
        if let subscriberNumber = phoneNumberField.text, let countryCode = countryCodeLabel.text {
            registration.phone = TelephoneNumber(countryCode: countryCode, subscriberNumber: subscriberNumber)
        } else {
            registration.phone = nil
        }
        
        // Update button text
        if registration.phoneIsVerified() {
            sendButton.setTitle("Next", for: .normal)
        } else {
            sendButton.setTitle("Send Verification Code", for: .normal)
        }
    }
    
}
