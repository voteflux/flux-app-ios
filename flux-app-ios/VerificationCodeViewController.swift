//
//  VerificationCodeViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit
import Alamofire
import Security

class VerificationCodeViewController: UIViewController, UITextFieldDelegate, RegistrationPageProtocol {

    // MARK: Constants
    
    /// Next stage of registration
    private let verificationCodeValidSegue = "verificationCodeValid"
    
    /// Length of SMS code to auto-submit on
    private let codeLength = 6;
    
    
    // MARK: RegistrationPageProtocol
    
    var registration: RegistrationForm {
        get {
            return (navigationController as? RegistrationNavigationController)!.form
        }
        set {
            (navigationController as? RegistrationNavigationController)!.form = newValue
        }
    }
    
    func meetsRequirements() -> Bool {
        return registration.phoneIsVerified()
    }
    
    
    // MARK: Outlets
    
    @IBOutlet weak var smsCodeField: UITextField!
    @IBAction func nextButton() {
        performSegue(withIdentifier: verificationCodeValidSegue, sender: nil)
    }
    
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = colors.backgroundColor
        
        // Do any additional setup after loading the view.
        smsCodeField.delegate = self;
        smsCodeField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Navigation
    
    /// Only perform segue if phone has been verified
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return meetsRequirements()
    }
    
    // MARK: - UITextFieldDelegate
    
    /// Limit textField to 6 characters, avoid copy-pasting over 6 characters
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // if 6 characters auto submit and block
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        return newLength <= codeLength
    }
    
    /// Watch for finished length
    func textFieldDidChange(_ textField: UITextField) {
        if let code = textField.text, code.characters.count == codeLength {
            verify(code: code)
        }
    }
    
    @objc func hideKeyboard() {
        smsCodeField.resignFirstResponder()
    }
    
    // MARK: - Registration
    
    /// Attempt to verify provided code.
    func verify(code: String) {
        registration.nonce = FluxAPI.generateNonce()
        if let phone = registration.phone, let nonce = registration.nonce {
            LoadingIndicator.standard.show(onView: view)
            FluxAPI.verifyPhone(phone: phone, code: code, nonce: nonce, onSuccess: onSuccess, onFail: onFail)
        } else {
            debugPrint("Verification of nonce: \(registration.nonce) phone: \(registration.phone?.internationalNumber) code: \(code) failed.")
        }
    }

    /// Stop animation, store phone verification, continue registration.
    func onSuccess(withPhone phone: TelephoneNumber, withNonce nonce: String) {
        LoadingIndicator.standard.hide()
        registration.setVerified(phone: phone, nonce: nonce)
        performSegue(withIdentifier: verificationCodeValidSegue, sender: nil)
    }
    
    /// Stop animation, presents alert with error.
    func onFail(message: String?) {
        LoadingIndicator.standard.hide()
        self.presentError(title: errorTitle, message: message, close: errorClose)
    }
    
}
