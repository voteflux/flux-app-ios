//
//  PersonalDetailsViewController.swift
//  flux-app-ios
//
//  Created by Mitchell Della Marta on 1/2/17.
//  Copyright © 2017 qfyueg. All rights reserved.
//

import UIKit

class PersonalDetailsViewController: UIViewController, UITextFieldDelegate, RegistrationPageProtocol {

    let addressDetailsSegue = "addressDetails"
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var middleNamesField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    // MARK: Preparation
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = colors.backgroundColor
        
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        // Delegates to watch handle transitions
        firstNameField.delegate = self
        middleNamesField.delegate = self
        surnameField.delegate = self
        emailField.delegate = self
        
        // Remove verification code from navigation stack
        if let i = self.navigationController?.viewControllers.index(where: { $0 is VerificationCodeViewController }) {
            self.navigationController?.viewControllers.remove(at: i)
        }
    }
    
    /// Loads stored values
    override func viewWillAppear(_ animated: Bool) {
        if let firstName = registration.firstName {
            firstNameField.text = firstName
        }
        if let middleNames = registration.middleNames {
            middleNamesField.text = middleNames
        }
        if let lastName = registration.lastName {
            surnameField.text = lastName
        }
        if let email = registration.email {
            emailField.text = email
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    /// Perform segue if registration requirements are met
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        registration.firstName = firstNameField.text
        registration.middleNames = middleNamesField.text
        registration.lastName = surnameField.text
        registration.email = emailField.text
//        registration.country = ""
        
        return meetsRequirements()
    }
    
    
    // MARK: UITextFieldDelegate
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    /// Transition between text fields
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameField {
            middleNamesField.becomeFirstResponder()
            firstNameField.resignFirstResponder()
        } else if textField == middleNamesField {
            surnameField.becomeFirstResponder()
            middleNamesField.resignFirstResponder()
        } else if textField == surnameField {
            emailField.becomeFirstResponder()
            surnameField.resignFirstResponder()
        } else if textField == emailField {
            emailField.resignFirstResponder()
        }
        
        return false
    }
    
    // MARK: - RegistrationPageProtocol
    
    var registration: RegistrationForm {
        get {
            return (navigationController as? RegistrationNavigationController)!.form
        }
        set {
            (navigationController as? RegistrationNavigationController)!.form = newValue
        }
    }
    
    /// Firstname, lastname and email are required
    func meetsRequirements() -> Bool {
        return registration.firstName != "" && registration.firstName != nil &&
            registration.lastName != "" && registration.lastName != nil &&
            registration.email != "" && registration.email != nil
    }

}
